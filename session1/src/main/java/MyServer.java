import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: chadmaughan
 * Date: 12/1/11
 * Time: 7:16 AM
 */
public class MyServer extends SimpleChannelHandler {

    // they wrapped all the channels in a group (that way you can close all of them at one point
    final static private ChannelGroup allMyChannels = new DefaultChannelGroup("MyServer");

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        System.out.println("conencted");
        super.channelConnected(ctx, e);
    }

    // if something goes wrong, you can catch it here
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        e.getCause().printStackTrace();
        super.exceptionCaught(ctx, e);
    }

    // how to receive a message
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {

        // I have no idea what he was talking about during this code block
        //    ChannelFuture future = e.getChannel().close();
        //    future.addListener(
        //        new ChannelFutureListener() {
        //            public void operationComplete(ChannelFuture channelFuture) throws Exception {
        //            }
        //        }
        //    );
        
        // another option is to just return
        // if you were doing socket programming, you have to worry about the flow
        // with netty, you don't have to, it'll take care of it
        //    e.getChannel().write(e.getMessage());

        // we're receiving raw bytes here, no objects
        //    ChannelBuffer buff = (ChannelBuffer) e.getMessage();

        // is this readable or writable?
        //    buff.readable();
        //    buff.writable();

        // how long is it?
        //    int length = buff.readableBytes();

        // looks at the byte but doesn't take it off the buffer
        // CTRL-D = ascii 4
        //    if(buff.getByte(0) == 4) {
        //        e.getChannel().close();
        //    }
        //    else {
        //        while(buff.readable()) {
        //            System.out.print((char) buff.readByte());
        //        }
        //    }

        // originally, we just printed out that we received a message
        //    System.out.println("got message");
        //    super.messageReceived(ctx, e);
    }

    public static void main(String[] args) {
        ChannelFactory factory = new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool());

        ServerBootstrap bootstrap = new ServerBootstrap(factory);

        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() throws Exception {
                return Channels.pipeline(new MyServer());
            }
        });

        // every client gets it's own channel
        bootstrap.setOption("child.keepAlive", true);

        Channel channel = bootstrap.bind(new InetSocketAddress(8080));

        // parent channel
        allMyChannels.add(channel);

        System.out.println("Server started...");

        System.out.println("enter something to quit");
        System.console().readLine();

        System.out.println("terminating server");

        // closes all connections (after the clients close off their connections)
        ChannelGroupFuture closeFuture = allMyChannels.close();

        // this waits forever, you can also pass it a time where it gets impatient and closes
        closeFuture.awaitUninterruptibly();
        
        channel.getCloseFuture().awaitUninterruptibly();

        factory.releaseExternalResources();
    }
}
