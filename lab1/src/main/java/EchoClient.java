import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: chadmaughan
 * Date: 12/1/11
 * Time: 8:33 AM
 */
public class EchoClient extends SimpleChannelHandler {

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        readAndSendDataToServer(e.getChannel());
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        ChannelBuffer buff = (ChannelBuffer) e.getMessage();
        while(buff.readable()) {
            // if you don't cast to a char, the ascii value comes out, oops
            System.out.print((char) buff.readByte());
        }

        readAndSendDataToServer(e.getChannel());
    }

    private void readAndSendDataToServer(Channel channel) {
        String input = System.console().readLine();
        if(input != null) {

            ChannelBuffer buff = ChannelBuffers.buffer(input.length());
            buff.writeBytes(input.getBytes());

            // push the buffer onto the channel
            channel.write(buff);
        }
        else {
            channel.close();
        }
    }

    public static void main(String[] args) {

        // a client only has one channel, whereas a server has multiple
        ChannelFactory factory = new NioClientSocketChannelFactory(
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool());

        ClientBootstrap bootstrap = new ClientBootstrap(factory);

        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() throws Exception {
                return Channels.pipeline(new EchoClient());
            }
        });

        // every client gets it's own channel
        bootstrap.setOption("child.keepAlive", true);

        ChannelFuture channelFuture = bootstrap.connect(new InetSocketAddress("localhost", 8080));

        System.out.println("Client started...");

        // when you send a ^D the server closes, we fall out of this awaitUninterruptibly and end the client
        channelFuture.awaitUninterruptibly();
        channelFuture.getChannel().getCloseFuture().awaitUninterruptibly();

        System.out.println("ending client");

        // this is what terminates the application
        factory.releaseExternalResources();

    }
}
