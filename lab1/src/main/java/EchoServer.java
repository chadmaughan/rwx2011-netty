import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: chadmaughan
 * Date: 12/1/11
 * Time: 8:33 AM
 */
public class EchoServer extends SimpleChannelHandler {

    // they wrapped all the channels in a group (that way you can close all of them at one point
    final static private ChannelGroup allMyChannels = new DefaultChannelGroup("EchoServer");

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        System.out.println("conencted client");
        super.channelConnected(ctx, e);
    }

    // if something goes wrong, you can catch it here
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        e.getCause().printStackTrace();
        super.exceptionCaught(ctx, e);
    }

    // how to receive a message
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {

        ChannelBuffer buff = (ChannelBuffer) e.getMessage();
        
        System.out.print((char) buff.readByte());
        while(buff.readable()) {
            System.out.print((char) buff.readByte());
        }

        if(buff.getByte(0) == 4) {
            e.getChannel().close();
        }
        else {
        }
    }

    public static void main(String[] args) {
        ChannelFactory factory = new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool());

        ServerBootstrap bootstrap = new ServerBootstrap(factory);

        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() throws Exception {
                return Channels.pipeline(new EchoServer());
            }
        });

        // every client gets it's own channel
        bootstrap.setOption("child.keepAlive", true);

        Channel channel = bootstrap.bind(new InetSocketAddress(8080));

        // parent channel
        allMyChannels.add(channel);

        System.out.println("Server started...");

        System.out.println("enter something to quit");
        System.console().readLine();

        System.out.println("terminating server");

        // closes all connections (after the clients close off their connections)
        ChannelGroupFuture closeFuture = allMyChannels.close();

        // this waits forever, you can also pass it a time where it gets impatient and closes
        closeFuture.awaitUninterruptibly();

        channel.getCloseFuture().awaitUninterruptibly();

        factory.releaseExternalResources();
    }
}
